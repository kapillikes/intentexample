package com.example.intentexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Intent intent=getIntent();
        String val=intent.getStringExtra("key");
        int value=intent.getIntExtra("a1",0);
        Toast.makeText(this, val+value, Toast.LENGTH_SHORT).show();
    }
}
