package com.example.intentexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button button,buttonImplicit,buttonSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.buttonClick);
        buttonImplicit=findViewById(R.id.buttonClickImplicit);
        buttonSend=findViewById(R.id.buttonClickSend);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra("key", "value");
                intent.putExtra("a1",1);
                startActivity(intent);
            }
        });

        buttonImplicit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Uri u=Uri.parse("tel:987654321");
                Intent i=new Intent(Intent.ACTION_DIAL);

                startActivity(i);
            }
        });
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Intent.ACTION_SEND);
               /* i.putExtra(Intent.EXTRA_EMAIL,new String[]{""});
                i.putExtra(Intent.EXTRA_SUBJECT,"hiiii");
                i.putExtra(Intent.EXTRA_TEXT,"jfhfhff");*/
                i.setType("text/plain");
                startActivity(i);
            }
        });

    }
}
